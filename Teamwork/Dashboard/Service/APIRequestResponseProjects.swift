//
//  APIRequestResponseProjects.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class APIRequestResponseProjects: APIBaseRequestResponse {

    override func route() -> String {
        return "/projects.json"
    }
    
    override func parseJSON(json: NSDictionary) -> AnyObject? {
        var retList = [Project]()
        if let list = json.valueForKeyPath("projects") as? [AnyObject]{
            for item in list{
                if let json = item as? NSDictionary{
                    retList.append(Project(jsonDictionary: json))
                }
            }
        }
        return retList
    }
}
