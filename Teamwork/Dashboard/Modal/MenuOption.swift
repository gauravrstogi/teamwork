//
//  MenuOption.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 09/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class MenuOption: BaseModel {
    var optionId : OptionID
    var title : String
    var optionImage : String?
    var optionHighlightedImage : String?
    var menuPosition : Int8
    
    override init(jsonDictionary: NSDictionary!) {
        optionId = OptionID(rawValue: (jsonDictionary["optionId"] as! NSNumber).integerValue)!
        title = jsonDictionary["title"] as! String
        optionImage = jsonDictionary.stringObjectForKey("optionImage")
        optionHighlightedImage = jsonDictionary.stringObjectForKey("optionHighlightedImage")
        menuPosition = Int8((jsonDictionary["menuPosition"] as! NSNumber).integerValue)
        super.init(jsonDictionary: jsonDictionary)
    }
}
