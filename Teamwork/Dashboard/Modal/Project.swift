//
//  Project.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

struct Category{
    var name:String?
    var id:String
    var color:String?
}

struct Defaults {
    var privacy:String?
}

struct Company {
    var name:String?
    var isOwner:Bool = false
    var id:String
}

class Project: BaseModel {
    var starred:Bool = false
    var replyByEmailEnabled:Bool = false
    var showAnnouncement:Bool = false
    var harvestTimersEnabled:Bool = false
    var status:ProjectStatus = .Active
    var subStatus:ProjectSubStatus = .None
    var defaultPrivacy:DefaultPrivacy = .Open
    var createdOn : String?
    var category:Category?
    var filesAutoNewVersion:Bool = false
    var logo:NSURL?
    var startDate:String?
    var id:String!
    var lastChangedOn:String?
    var endDate:String?
    var defaults:Defaults?
    var company:Company?
    var name: String?
    var privacyEnabled:Bool = false
    var descriptionString : String?
    var announcement:String?
    var isProjectAdmin:Bool = false
    var startPage : String?
    var notifyeveryone:Bool = false
    var announcementHTML:String?
    
    override init(jsonDictionary: NSDictionary!) {
        if let dict = jsonDictionary{
            starred = dict.boolValueForKey("")
            replyByEmailEnabled = dict.boolValueForKey("")
            showAnnouncement = dict.boolValueForKey("show-announcement")
            harvestTimersEnabled = dict.boolValueForKey("harvest-timers-enabled")
            status = Project.getProjectStatus(dict.stringObjectForKey("status")!)
            subStatus = Project.getProjectSubStatus(dict.stringObjectForKey("subStatus")!)
            defaultPrivacy = Project.getDefaultPrivacy(dict.stringObjectForKey("defaultPrivacy")!)
            createdOn = dict.stringObjectForKey("created-on")
            
            if let categoryDictionary = dict.dictionaryObjectForKey("category"){
                category = Category(name: categoryDictionary.stringObjectForKey("name"), id: categoryDictionary.stringObjectForKey("name")!, color: categoryDictionary.stringObjectForKey("name"))
            }
            
            filesAutoNewVersion = dict.boolValueForKey("filesAutoNewVersion")
            logo = NSURL(string: dict.stringObjectForKey("logo")!)
            startDate = dict.stringObjectForKey("startDate")
            id = dict.stringObjectForKey("id")
            lastChangedOn = dict.stringObjectForKey("last-changed-on")
            endDate = dict.stringObjectForKey("endDate")
            defaults = Defaults(privacy: dict.dictionaryObjectForKey("defaults")?.stringObjectForKey("privacy"))
            
            if let companyDictionary = dict.dictionaryObjectForKey("company"){
                company = Company(name: companyDictionary.stringObjectForKey("name"), isOwner: companyDictionary.boolValueForKey("is-owner"), id: companyDictionary.stringObjectForKey("id")!)
            }
            
            name = dict.stringObjectForKey("name")
            privacyEnabled = dict.boolValueForKey("privacyEnabled")
            descriptionString = dict.stringObjectForKey("description")
            announcement = dict.stringObjectForKey("announcement")
            isProjectAdmin = dict.boolValueForKey("isProjectAdmin")
            startPage = dict.stringObjectForKey("start-page")
            notifyeveryone = dict.boolValueForKey("announcementHTML")
            announcementHTML = dict.stringObjectForKey("announcementHTML")
            
        }
        
        super.init(jsonDictionary: jsonDictionary)
    }
    
    class func getProjectStatus(string:String)->ProjectStatus{
        switch string {
        case "archved":
            return ProjectStatus.Archived
        default:
            return ProjectStatus.Active
        }
    }
    
    class func getProjectSubStatus(string:String)->ProjectSubStatus{
        switch string {
        case "current":
            return ProjectSubStatus.Current
        default:
            return ProjectSubStatus.Past
        }
    }
    
    class func getDefaultPrivacy(string:String)->DefaultPrivacy{
        switch string {
        case "open":
            return DefaultPrivacy.Open
        default:
            return DefaultPrivacy.OwnerCompany
        }
    }
}
