//
//  MenuOptionCell.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 09/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class MenuOptionCell: UITableViewCell {

    @IBOutlet var optionNameLabel:UILabel!
    @IBOutlet var optionImageView:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
