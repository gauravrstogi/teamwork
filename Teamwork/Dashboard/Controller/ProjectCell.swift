//
//  ProjectCell.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import SDWebImage

class ProjectCell: UICollectionViewCell {
    
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var projectNameLabel: UILabel!
    @IBOutlet var starImageView:UIImageView!
    
    func updateContentOfCell(project:Project){
        if let logoUrl = project.logo{
            self.logoImageView!.sd_setImageWithURL(logoUrl, placeholderImage: UIImage(named: "project-default"), options: .LowPriority)
        }
        else{
            self.logoImageView.image = nil
        }
        
        if let name = project.name{
            projectNameLabel.text = name
        }
        else{
            projectNameLabel.text = ""
        }
        
        if project.starred == true{
            starImageView.image = UIImage(named: "star")
        }
        else{
            starImageView.image = nil
        }
    }
}
