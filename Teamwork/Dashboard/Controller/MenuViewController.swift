//
//  MenuViewController.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 09/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import SDWebImage
import SWRevealViewController

class MenuViewController: BaseViewController , UITableViewDelegate, UITableViewDataSource{

    @IBOutlet var companyNameLabel: UILabel!
    @IBOutlet var accountHolderNameLabel: UILabel!
    @IBOutlet var profileImageView: UIImageView!
    
    var account:Account?
    var menuOptions:[MenuOption]? 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let optionSortDescriptor = NSSortDescriptor(key: "menuPosition", ascending: true)
        menuOptions = Configuration.fetchSortedMenuItems(optionSortDescriptor)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.account = StorageManager.sharedInstance.currentAccount
        setupHeaderUI()
    }

    func setupHeaderUI(){
        if let logo = self.account?.logo{
            self.profileImageView.sd_setImageWithURL(logo, placeholderImage: UIImage(named: "ic_user-default"), options: SDWebImageOptions.LowPriority)
        }
        
        if let displayName = self.account?.displayNameString(){
            self.accountHolderNameLabel.text = displayName
        }
        
        if let compantName = self.account?.companyname{
            companyNameLabel.text = compantName
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableView Delegate & DataSource Methods -
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let options = self.menuOptions{
            return options.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:MenuOptionCell = tableView.dequeueReusableCellWithIdentifier("menuOptionCell")! as! MenuOptionCell
        
        let menuOption : MenuOption = self.menuOptions![indexPath.row]
        cell.optionNameLabel.text = menuOption.title
        if let optionImageStr = menuOption.optionImage
        {
            cell.optionImageView.image = UIImage(named: optionImageStr)
        }
        else
        {
            cell.optionImageView.image = nil
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let menuOption = self.menuOptions![indexPath.row]
        
        switch menuOption.optionId {
        case .Projects:
            break
        case .Settings:
            break
        default:
            break
        }
        self.revealViewController().revealToggleAnimated(true)
    }

}
