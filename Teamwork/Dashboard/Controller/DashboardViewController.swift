//
//  DashboardViewController.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import MBProgressHUD
import SWRevealViewController

class DashboardViewController: BaseViewController , UICollectionViewDelegate, UICollectionViewDataSource ,SWRevealViewControllerDelegate{

    @IBOutlet var menuButton: UIBarButtonItem!
    var projects:[Project]? = nil
    @IBOutlet var projectGrid:UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        customSetup()
        getProject()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customSetup(){
        if revealViewController() != nil
        {
            revealViewController().rearViewRevealWidth = (screenWidth/5)*4
            revealViewController().delegate = self
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        }
    }
    
    func getProject(){
        let progressHUD:MBProgressHUD = MBProgressHUD()
        progressHUD.label.text = "Syncing Projects"
        progressHUD.detailsLabel.text = "Please wait .."
        self.view.addSubview(progressHUD)
        progressHUD.showAnimated(true)
        NetworkManager.sharedInstance.getProjects {[unowned self] (error, responseData) in
            if error == nil{
                if let projectsList = responseData{
                    self.projects = projectsList
                    self.projectGrid.reloadData()
                }
            }
            else{
                if !self.handleHTTPError(error!){
                    UIUtility.showAlertWithTitle(localize("ALERT"),
                        message: localize((error?.localizedDescription)!),
                        viewController: self)
                }
            }
        }
    }
    
    //MARK: - UICollectionView Delegate & DataSource Methods -
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if let list = self.projects{
            return list.count
        }
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("projectCell", forIndexPath: indexPath) as! ProjectCell
        if let list = self.projects{
            cell.updateContentOfCell(list[indexPath.row])
        }
        return cell
    }
}
