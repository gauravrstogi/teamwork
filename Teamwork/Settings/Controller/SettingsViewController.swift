//
//  SettingsViewController.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 09/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import SWRevealViewController

class SettingsViewController: BaseViewController, SWRevealViewControllerDelegate {

    @IBOutlet var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()

        // Do any additional setup after loading the view.
    }

    func customSetup(){
        if revealViewController() != nil
        {
            revealViewController().rearViewRevealWidth = (screenWidth/5)*4
            revealViewController().delegate = self
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
