//
//  APIRequestFactory.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class APIRequestFactory: NSObject {
    class func requestWithID(requestId:RequestType)->APIBaseRequestResponse?{
        switch (requestId){
        case .RequestTypeAuthenticate:
            return APIRequestResponseAuthenticate(requestId: requestId)
        case .RequestTypeProjects:
            return APIRequestResponseProjects(requestId: requestId)
        default:
            return nil
        }
    }
}
