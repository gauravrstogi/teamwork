//
//  NetworkManager.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class NetworkManager: NSObject {

    class var sharedInstance: NetworkManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: NetworkManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = NetworkManager()
        }
        return Static.instance!
    }
    
    func authenticate(completionHandler:(error:NSError?, responseData:Account?)->Void)->Void{
        let requestResponse:APIBaseRequestResponse = APIRequestFactory.requestWithID(.RequestTypeAuthenticate)!
        if let request = requestResponse as? APIRequestResponseAuthenticate{
            request.apiKey = Configuration.sharedInstance.apiKey
            request.password = "X"
            request.executeRequest { (error, response) -> Void in
                completionHandler(error: error, responseData: response as? Account)
            }
        }
    }
    
    func getProjects(completionHandler:(error:NSError?, responseData:[Project]?)->Void)->Void{
        let requestResponse:APIBaseRequestResponse = APIRequestFactory.requestWithID(.RequestTypeProjects)!
        if let request = requestResponse as? APIRequestResponseProjects{
            request.executeRequest { (error, response) -> Void in
                completionHandler(error: error, responseData: response as? [Project])
            }
        }
    }
}
