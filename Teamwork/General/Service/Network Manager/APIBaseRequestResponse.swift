//
//  APIBaseRequestResponse.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import AFNetworking

typealias requestCallbackClosure = (error:NSError?, responseObject:AnyObject?)->Void
typealias successClosure = (task:NSURLSessionDataTask, data:AnyObject?, requestCallbackClosure) -> Void
typealias failureClosure = (task:NSURLSessionDataTask?, error:NSError, requestCallbackClosure) -> Void

class APIBaseRequestResponse: NSObject {

    var requestId:RequestType = .RequestTypeUnknown
    var requestMethod:RequestMethod?
    var parameterDictionary:NSMutableDictionary?
    var error:NSError?
    
    var isCancelled:Bool!
    
    var currentTask:NSURLSessionDataTask?
    let sessionManager = AFHTTPSessionManager()
    
    var baseURL:String{
        get{
            return "https://"+Configuration .sharedInstance .hostURL
        }
    }
    
    init(requestId:RequestType, requestMethod:RequestMethod = .RequestMethodGet) {
        
        self.requestId          = requestId
        self.requestMethod      = requestMethod
        self.isCancelled        = false
        
        sessionManager.requestSerializer = AFHTTPRequestSerializer()
        sessionManager.responseSerializer = AFHTTPResponseSerializer()

        super.init()
    }
    func headers()->[String : String]?{
        return ["Accept":"application/json"]
    }
    func parameters()->[String:AnyObject]?{
        return nil
    }
    func route()->String{
        return ""
    }
    func parseJSON(json:NSDictionary)->AnyObject?{
        return nil
    }
    func getServiceURL()->String{
        let url: String = baseURL + route()
        logger.debug("Request Url is \(url)")
        return url
    }
    func handleSuccessBlock(task:NSURLSessionDataTask, data:AnyObject?, completion:requestCallbackClosure){
        if ((data?.isKindOfClass(NSData)) == nil){
            return
        }
        let responseString = String(data: data as! NSData, encoding: NSUTF8StringEncoding)
        logger.debug(responseString)
        do{
            let jsonObject = try NSJSONSerialization.JSONObjectWithData(data as! NSData, options: [])
            
            if let json = jsonObject as? NSDictionary{
                if(json["error"] as? String == nil){//Server response success
                    dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), {() -> Void in
                        if let parsedData = self.parseJSON(json){
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                completion(error: nil, responseObject:parsedData)
                            })
                        }
                        else{
                            let userInfo: [NSObject : AnyObject] =
                                [
                                    NSLocalizedDescriptionKey :  NSLocalizedString("ErrorMessage", value: "No data", comment: ""),
                                    NSLocalizedFailureReasonErrorKey : NSLocalizedString("ErrorMessage", value: "No Data", comment: "")
                            ]
                            let err = NSError(domain: ErrorDomain, code: 200, userInfo: userInfo)
                            logger.error("Parse data Error: \(err.localizedDescription)")
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                completion(error: err, responseObject: nil)
                            })
                        }
                        }
                    )
                }
                else{
                    //Server Error
                    let errorMsg:String = json["error"] as! String
                    let userInfo: [NSObject : AnyObject] =
                        [
                            NSLocalizedDescriptionKey :  NSLocalizedString("ErrorMessage", value: errorMsg, comment: ""),
                            NSLocalizedFailureReasonErrorKey : NSLocalizedString("ErrorMessage", value: errorMsg, comment: "")
                    ]
                    let err = NSError(domain: ErrorDomain, code: 200, userInfo: userInfo)
                    logger.error("API Response Error: \(err.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(error: err, responseObject: nil)
                        if errorMsg.caseInsensitiveCompare("Invalid Token") == NSComparisonResult.OrderedSame{
                            //GLOUIUtility.showLoginWithResetInfo(true)
                        }
                    })
                    
                }
                
            }
        } catch let err as NSError {
            logger.error("API Response Error: \(err.localizedDescription)")
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completion(error: err, responseObject: nil)
            })
        }
    }
    
    func handleFailureBlock(task:NSURLSessionDataTask?, error:NSError?, completion:requestCallbackClosure){
        if error != nil{
            if let response = task!.response as? NSHTTPURLResponse{
                logger.error("Response statusCode: \(response.statusCode)")
            }
            if error!.code == -999{
                return
            }
            
            if let errorData = error!.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? NSData{
                do{
                    if let responseString = String(data: errorData, encoding: NSUTF8StringEncoding){
                        logger.error("API Response Http Error Data: \(responseString)")
                    }
                    else{
                        logger.error("API Response Http Error Data: No Response String")
                    }
                    let jsonObject = try NSJSONSerialization.JSONObjectWithData(errorData, options: [])
                    if let json = jsonObject as? NSDictionary{
                        if let error = json["error"] as? String{
                            if error.caseInsensitiveCompare("Invalid Token") == NSComparisonResult.OrderedSame{
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    //GLOUIUtility.showLoginWithResetInfo(true)
                                })
                                return
                            }
                            else{
                                let userInfo: [NSObject : AnyObject] =
                                    [
                                        NSLocalizedDescriptionKey :  NSLocalizedString("ErrorMessage", value: error, comment: ""),
                                        NSLocalizedFailureReasonErrorKey : NSLocalizedString("ErrorMessage", value: error, comment: "")
                                ]
                                let err = NSError(domain: ErrorDomain, code: 200, userInfo: userInfo)
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    completion(error: err, responseObject: nil)
                                })
                                return
                            }
                        }
                    }
                } catch let err as NSError {
                    logger.error("API Response Error: \(err.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(error: err, responseObject: nil)
                    })
                    return
                }
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completion(error: error, responseObject: nil)
            })
        }
        else{
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completion(error: nil, responseObject: nil)
            })
        }
    }
    func executeRequest(completionHandler:(error:NSError?, modelObject:AnyObject?)->Void)->Void{
        if let headerValues = self.headers(){
            for header in headerValues{
                sessionManager.requestSerializer.setValue(header.1, forHTTPHeaderField: header.0)
            }
        }
        
        //Adding Authorization and Credential
        sessionManager.requestSerializer.setAuthorizationHeaderFieldWithUsername(Configuration.sharedInstance.apiKey, password: Configuration.sharedInstance.password)
        
        switch requestMethod! as RequestMethod{
        case .RequestMethodPost:
            currentTask = sessionManager.POST(getServiceURL(),
                                              parameters: self.parameters(),
                                              progress:nil,
                                              success: { (task, data) -> Void in
                                                self.handleSuccessBlock(task, data: data, completion: completionHandler)
                },
                                              failure: { (task, error) -> Void in
                                                self.handleFailureBlock(task, error:error, completion:completionHandler)
            })
            break
        default:
            currentTask = sessionManager.GET(getServiceURL(),
                                             parameters: self.parameters(),
                                             progress:nil,
                                             success: { (task, data) -> Void in
                                                self.handleSuccessBlock(task, data: data, completion: completionHandler)
                },
                                             failure: {(task, error) -> Void in
                                                self.handleFailureBlock(task, error:error, completion:completionHandler)
            })
            break
        }
    }
}
