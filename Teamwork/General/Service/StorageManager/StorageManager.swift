//
//  StorageManager.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 09/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class StorageManager: NSObject {
    private var account:Account? = nil
    class var sharedInstance: StorageManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: StorageManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = StorageManager()
        }
        return Static.instance!
    }
    
    var currentAccount:Account?{
        get{
            if account != nil{
                return account
            }
            
            return nil
        }
        set (newAccount){
            if let nAccount = newAccount{
                self.account = nAccount
            }
        }
    }

}
