//
//  UIUtility.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import Foundation
import MBProgressHUD

class UIUtility: NSObject {
    
    class func showAlertWithTitle(title:String , message:String , buttonTitles:[String], viewController:UIViewController , actionHandler:((buttonIndex:Int)->Void)?)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        for strTitle:String in buttonTitles{
            alert.addAction(UIAlertAction(title:strTitle, style: UIAlertActionStyle.Default, handler: {
                (alert: UIAlertAction!) in
                
                let index = buttonTitles.indexOf( { (str: String) -> Bool in
                    return str == alert.title
                })
                actionHandler?(buttonIndex:index!)
            }))
        }
        
        viewController.presentViewController(alert, animated: true, completion: nil)
    }
    
    class func showAlertWithTitle(title:String , message:String , viewController:UIViewController)
    {
        UIUtility.showAlertWithTitle(title, message: message, buttonTitles: [localize("OK")], viewController: viewController, actionHandler: nil)
    }
    
    class func showToastWithMessage(message:String){
        let progressHud:MBProgressHUD = MBProgressHUD()
        progressHud.mode = MBProgressHUDMode.Text
        progressHud.detailsLabel.text = message
        progressHud.detailsLabel.font = UIFont.systemFontOfSize(12.0)
        progressHud.margin = 10
        progressHud.offset.y = CGFloat((screenHeight/2)-40)
        progressHud.userInteractionEnabled = false
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.addSubview(progressHud)
        progressHud.showAnimated(true)
        progressHud.removeFromSuperViewOnHide = true
        progressHud.hideAnimated(true, afterDelay: 3)
    }
}
