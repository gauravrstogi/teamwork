//
//  BaseModal.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class BaseModel: NSObject {

    init(jsonDictionary:NSDictionary!) {
        super.init()
    }
    
    override init() {
        super.init()
    }
}
