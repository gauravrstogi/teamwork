//
//  Constants.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import XCGLogger

enum RequestType: Int{
    case RequestTypeUnknown = 0,
    RequestTypeAuthenticate,
    RequestTypeProjects
}

enum RequestMethod : Int {
    case RequestMethodPost=0,
    RequestMethodGet
}

enum ProjectStatus : Int {
    case Active = 0 ,
    Archived
}

enum ProjectSubStatus : Int {
    case Current = 0,
    Past,
    None
}

enum DefaultPrivacy : Int{
    case Open = 0,
    OwnerCompany
}

enum OptionID: Int{
    case Projects = 0,
    Settings,
    Account,
    Profile
}
 

var screenWidth:CGFloat{
get{
    return (UIApplication.sharedApplication().windows.first?.frame.width)!
}
}

var screenHeight:CGFloat{
get{
    return (UIApplication.sharedApplication().windows.first?.frame.height)!
}

}

func localize (key:String) ->String
{
    return NSLocalizedString(key, comment: "")
}

let logger = XCGLogger.defaultInstance()
let ErrorDomain = "com.gauravrstogi.teamwork"