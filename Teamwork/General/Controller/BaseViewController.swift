//
//  BaseViewController.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Network Sevice methods
    func handleHTTPError(error:NSError) -> Bool{
        if error.domain == ErrorDomain{
            return false
        }
        switch( error.code){
        case NSURLErrorUserAuthenticationRequired:
            UIUtility.showToastWithMessage(localize("USER_AUTHENTICATION_ERROR"))
            break
        case NSURLErrorNetworkConnectionLost,
             NSURLErrorNotConnectedToInternet:
            UIUtility.showToastWithMessage(localize("HTTP_ERROR_NO_INTERNET"))
            break
        case NSURLErrorBadURL:
            break
        case NSURLErrorTimedOut:
            UIUtility.showToastWithMessage(localize("HTTP_ERROR_REQ_TIMEOUT"))
            break
        case NSURLErrorCannotFindHost,
             NSURLErrorCannotConnectToHost:
            UIUtility.showToastWithMessage(localize("HTTP_ERROR_INVALID_HOST"))
            break
        case NSURLErrorServerCertificateHasBadDate,
             NSURLErrorServerCertificateUntrusted,
             NSURLErrorServerCertificateHasUnknownRoot,
             NSURLErrorServerCertificateNotYetValid,
             NSURLErrorClientCertificateRejected,
             NSURLErrorClientCertificateRequired:
            UIUtility.showToastWithMessage(localize("HTTP_ERROR_INVALID_CERT"))
            break
        case NSURLErrorBadServerResponse:
            UIUtility.showToastWithMessage(localize("INTERNAL_SERVER_ERROR"))
            break
        default:
            UIUtility.showToastWithMessage(localize("HTTP_ERROR_UNHANDELED_ERROR"))
            break
        }
        return true
    }


}
