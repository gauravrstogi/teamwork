//
//  NSDictionary+Extensions.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import Foundation

extension NSDictionary{
    func stringObjectForKey(key:String)->String?{
        if let obj:AnyObject = self.objectForKey(key){
            if obj is String{
                let str = obj as! String
                if str.characters.count > 0 {
                    return str
                }
            }
            else if obj is NSNumber{
                let number = obj as! NSNumber
                return number.stringValue
            }
        }
        return nil
    }
    
    func dictionaryObjectForKey(key:String)->NSDictionary?{
        
        if let obj:AnyObject = self.objectForKey(key){
            if obj is NSDictionary{
                return (obj as! NSDictionary)
            }
        }
        return nil
    }
    
    func boolValueForKey(key:String) -> Bool{
        if let obj:AnyObject = self.objectForKey(key){
            if obj is NSNumber{
                return (obj as! NSNumber).boolValue
            }
        }
        return false
    }
    
    func numberObjectForKey(key:String)->NSNumber?{
        
        if let obj:AnyObject = self.objectForKey(key){
            if obj is NSNumber{
                return (obj as! NSNumber)
            }
        }
        return nil
    }
    
    func arrayObjectForKey(key:String)->NSArray?{
        
        if let obj:AnyObject = self.objectForKey(key){
            if obj is NSArray{
                return (obj as! NSArray)
            }
        }
        return nil
    }
    
}
