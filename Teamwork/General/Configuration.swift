//
//  Configuration.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import Foundation

class Configuration: NSObject {
    
    var configDict : NSDictionary!
    var currentEnvironmentDict : NSDictionary!
    
    class var sharedInstance: Configuration {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: Configuration? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = Configuration()
            let path = NSBundle.mainBundle().pathForResource("Config", ofType: "plist")
            let dict = NSDictionary(contentsOfFile: path!)
            Static.instance!.configDict = dict
            let currentEnv = Static.instance!.currentEnvironment
            Static.instance!.currentEnvironmentDict = dict!.valueForKey(currentEnv) as! NSDictionary
        }
        return Static.instance!
    }
    
    var currentEnvironment:String{
        get{
            if let val = NSUserDefaults .standardUserDefaults() .valueForKey("isProd") as? NSNumber{
                if val.boolValue == true{
                    return "Prod"
                }
            }
            return self.configDict!.valueForKey("CurrentEnvironment") as! String
        }
    }
    
    func isProduction() -> Bool{
        return self.currentEnvironment == "Prod"
    }
    
    var password:String{
        get{
            return self.currentEnvironmentDict!.valueForKey("Password") as! String
        }
    }
    
    var apiKey:String{
        get{
            return self.currentEnvironmentDict!.valueForKey("APIKey") as! String
        }
    }
    
    var hostURL:String{
        get{
            return self.currentEnvironmentDict!.valueForKey("URL") as! String
        }
    }
    
    class func fetchSortedMenuItems(sortDes:NSSortDescriptor) -> [MenuOption]{
        let path = NSBundle.mainBundle().pathForResource("MenuOptions", ofType: "plist")
        var menuOptionsArray = [MenuOption]()
        let resultArray = NSArray(contentsOfFile: path!) as! [Dictionary<String, AnyObject>]
        for i in 0 ..< resultArray.count{
            if let json = resultArray[i] as NSDictionary! {
                menuOptionsArray.append(MenuOption(jsonDictionary: json))
            }
        }
        let validOptions = (menuOptionsArray as NSArray).filter({
            let item = $0 as? MenuOption
            let number = item!.valueForKey(sortDes.key!) as! NSNumber
            return number.intValue >= 0
        })
        
        return (validOptions as NSArray).sortedArrayUsingDescriptors([sortDes]) as! [MenuOption]
    }
}
