//
//  APIRequestResponseLogin.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class APIRequestResponseAuthenticate: APIBaseRequestResponse {
    var apiKey:String!
    var password:String!
    
    override func route()->String{
        return "/account.json"
    }
    
    override func getServiceURL() -> String {
        return "https://authenticate.teamworkpm.net/authenticate.json"
    }
    
    override func parseJSON(json: NSDictionary) -> AnyObject? {
        if let dict = json.objectForKey("account") as? NSDictionary{
            return Account(jsonDictionary: dict)
        }
        return nil
    }
}
