//
//  Account.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Account: BaseModel {
    
    var userIsMemberOfOwnerCompany:Bool = false
    var tagsLockedToAdmins:Bool = false
    var firstname:String?
    var dateSeperator:String?
    var logo:NSURL?
    var chatEnabled:Bool = false
    var id:String!
    var startonsundays:Bool = false
    var tkoEnabled:Bool = false
    var tagsEnabled:Bool = false
    var url:String?
    var dateFormat:String?
    var code:String?
    var requirehttps:Bool = false
    var canaddprojects:Bool = false
    var name:String?
    var userIsAdmin:Bool = false
    var companyname:String?
    var canManagePeople:Bool = false
    var sslEnabled:Bool = true
    var documentEditorEnabled:Bool = false
    var projectsEnabled:Bool = false
    var timeFormat:String?
    var avatarUrl:String?
    var deskEnabled:Bool = false
    var likesEnabled:Bool = false
    var companyid:String?
    var lang:String?
    var lastname:String?
    var userId:String!
    var planId:String?
    
    override init(jsonDictionary: NSDictionary!) {
        if let dict = jsonDictionary{
            userIsMemberOfOwnerCompany = dict.boolValueForKey("userIsMemberOfOwnerCompany")
            tagsLockedToAdmins = dict.boolValueForKey("tagsLockedToAdmins")
            firstname = dict.stringObjectForKey("firstname")
            dateSeperator = dict.stringObjectForKey("dateSeperator")
            if let url = dict.stringObjectForKey("logo"){
                logo = NSURL(string: url)
            }
            chatEnabled = dict.boolValueForKey("chatEnabled")
            id = dict.stringObjectForKey("id")
            startonsundays = dict.boolValueForKey("startonsundays")
            tkoEnabled = dict.boolValueForKey("TKOEnabled")
            tagsEnabled = dict.boolValueForKey("tagsEnabled")
            url = dict.stringObjectForKey("URL")
            dateFormat = dict.stringObjectForKey("dateFormat")
            code = dict.stringObjectForKey("code")
            requirehttps = dict.boolValueForKey("requirehttps")
            canaddprojects = dict.boolValueForKey("canaddprojects")
            name = dict.stringObjectForKey("name")
            userIsAdmin = dict.boolValueForKey("userIsAdmin")
            companyname = dict.stringObjectForKey("companyname")
            canManagePeople = dict.boolValueForKey("canManagePeople")
            sslEnabled = dict.boolValueForKey("ssl-enabled")
            documentEditorEnabled = dict.boolValueForKey("documentEditorEnabled")
            projectsEnabled = dict.boolValueForKey("projectsEnabled")
            timeFormat = dict.stringObjectForKey("timeFormat")
            avatarUrl = dict.stringObjectForKey("avatar-url")
            deskEnabled = dict.boolValueForKey("deskEnabled")
            likesEnabled = dict.boolValueForKey("likesEnabled")
            companyid = dict.stringObjectForKey("companyid")
            lang = dict.stringObjectForKey("lang")
            lastname = dict.stringObjectForKey("lastname")
            userId = dict.stringObjectForKey("userId")
            planId = dict.stringObjectForKey("plan-id")
        }
        super.init(jsonDictionary: jsonDictionary)
    }
    
    func displayNameString() -> String?{
        if firstname != nil && lastname != nil{
            return firstname! + " " + lastname!
        }
        else if firstname != nil{
            return firstname
        }
        else if lastname != nil{
            return lastname
        }
        else{
            return nil
        }
    }
}
