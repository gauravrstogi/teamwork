//
//  ViewController.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 07/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import MBProgressHUD
import SWRevealViewController

class AuthenticateViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didClickAuthenticate() {
        let progressHUD:MBProgressHUD = MBProgressHUD()
        progressHUD.label.text = "Authenticating"
        progressHUD.detailsLabel.text = "Please wait .."
        self.view.addSubview(progressHUD)
        progressHUD.showAnimated(true)
        NetworkManager.sharedInstance.authenticate { (error, responseData) in
            if error == nil{
                if let account = responseData{
                    StorageManager.sharedInstance.currentAccount = account
                    let dashboardStoryboard = UIStoryboard(name: "Dashboard", bundle: nil)
                    let viewController = dashboardStoryboard.instantiateInitialViewController() as! SWRevealViewController
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.window?.rootViewController = viewController
                }
                else{
                    if !self.handleHTTPError(error!){
                        UIUtility.showAlertWithTitle(localize("ALERT"),
                            message: localize((error?.localizedDescription)!),
                            viewController: self)
                    }
                }
            }
            progressHUD.hideAnimated(true)
        }
    }

}

